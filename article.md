---
title: "Jak se liší trénovaná běžkyně od amatérky? Olympijský běh změřily senzory"
perex: "Svaly profesionálního běžce jsou uvyklé dlouhodobé námaze, díky rovnovážnému prostředí v organismu je pak tělo takového sportovce méně stresované, než je tomu u amatéra. Rozdíly ukázala analýza dat z desetikilometrového závodu."
description: "Opakujeme datový experiment z pražského maratonu. Tentokrát sledujeme, jak se liší zvládání tělesné zátěže u amatérské a profesionální sportovkyně."
authors: ["Jan Cibulka", "Marcel Šulek"]
published: "22. června 2016"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
url: "olympijsky-beh"
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
---

Ve středu 22. června se uskutečnil v 51 městech Česka Olympijský běh.  Přihlásilo se rekordních víc než  52 tisíc běžců a běžkyň, dvě z nich nesly speciální senzory tepu a okysličení svalů. Profesionální českou běžkyni Evu Vrabcovou-Nývltovou a amatérku Babetu Schneiderovou sledovaly senzory: Konkrétně hrudní pás, jaký běžci už vcelku běžně používají, a senzor okysličení a prokrvení svalu. Z kombinace všech těch měřených hodnot je možné odhadovat, jak tělo toho běžce reaguje na zátěž a kde má například mezery v tréninku.

*Plnou čarou se zobrazují hodnoty Evy Nývltové, čárkovaně Babety Schneiderové.*

<aside class="big">
  <iframe src="https://samizdat.cz/data/maraton-frontend/www/#map" frameborder="0" height="600" width="1024"></iframe>
</aside>
<aside class="big">
  <iframe src="https://samizdat.cz/data/maraton-olymp-frontend-graphs/www/" frameborder="0" height="600" width="1024"></iframe>
</aside>

Naměřené hodnoty komentoval pro Český rozhlas sportovní lékař Jiří Dostal, který se právě moderní analýze ve sportu věnuje.

Rozdíl mezi profesionálkou a amatérkou se ukázal chvíli po startu. Trénovaná maratonská běžkyně Eva Nývltová měla tep těsně po startu 170 úderů za minutu, stehenní sval na pravé noze pak zvládal spotřebovat asi 60 procent dodaného kyslíku. S tím, jak běh pokračoval, se jít tep postupně ustálil na 160 úderech za minutu a efektivita spalování kyslíku se vyšplhala k 90 procentům. Hodnoty zůstaly obdobné až do 15. minuty, kdy povolilo upevnění svalového senzoru, a zastavil se sběr dat.

Amatérka Babeta odstartovala s podobnou tepovou frekvencí, sval ale zvládal spálit jen 40 % dodaného kyslíku. Vystresovaný oběhový systém reagoval zvyšováním tepové frekvence, a to až na 182 úderů za minutu v 15. minutě běhu, přitom samotný sval nezvládal dodaný kyslík využít, spotřebovával jen okolo 20 procent.

„Dlouhodobě trénovaný sportovec má vybudovanou dostatečnou strukturální připravenost svalů,“ říká k tomu sportovní lékař Jiří Dostal. Zjednodušeně řečeno, trénovaný sval má dostatek nástrojů (jak vlásečnic pro přivádění krve, tak i třeba mitochondrií v buňkách) pro přeměnu dodaného kyslíku na energii. Pro budování téhle struktury je podle Dostala obzvlášť důležitá sportovní zátěž během puberty.

U méně trénovaného běžce pak zmíněné struktury chybí, sval tak není schopný dodaný kyslík využít. Tělo reaguje zvyšováním tepové frekvence, což sice zaplaví krev kyslíkem, ten ale zůstane opět bez využití. Organismus se tak nadbytečně stresuje a stav běžce se zhoršuje. Trénovaný sportovec se naopak dostane do rovnovážného stavu, ve kterém během výkonu vydrží relativně „v klidu“.

Obdobný experiment proběhl už při Pražském maratonu, kdy Český rozhlas [přenášel detaily z běhu hobby běžce Alexe Trejtnara](http://www.rozhlas.cz/zpravy/data/_zprava/jak-boli-maraton-senzory-zmeri-co-se-deje-s-telem-svatecniho-bezce--1610964), který úspěšně doběhl právě i díky konzultaci se sportovním lékařem.